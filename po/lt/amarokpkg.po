# Lithuanian translations for amarokpkg package.
#
# Tomas Straupis <tomasstraupis@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: amarokpkg\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 11:43+0100\n"
"PO-Revision-Date: 2010-12-29 22:14+0200\n"
"Last-Translator: Tomas Straupis <tomasstraupis@gmail.com>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 1.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrius Štikonas"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrius@stikonas.eu"

#: amarokpkg.cpp:40
#, kde-format
msgid "Install, list, remove Amarok applets"
msgstr "Įdiegti, rodyti, šalinti Amarok įskiepius"

#: amarokpkg.cpp:76
#, kde-format
msgid "Amarok Applet Manager"
msgstr "Amarok įskiepių tvarkyklė"

#: amarokpkg.cpp:78
#, kde-format
msgid "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"
msgstr "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"

#: amarokpkg.cpp:79
#, kde-format
msgid "Aaron Seigo"
msgstr "Aaron Seigo"

#: amarokpkg.cpp:80
#, kde-format
msgid "Original author"
msgstr "Pradinis autorius"

#: amarokpkg.cpp:82
#, kde-format
msgid "Leo Franchi"
msgstr "Leo Franchi"

#: amarokpkg.cpp:83
#, kde-format
msgid "Developer"
msgstr "Programuotojas"

#: amarokpkg.cpp:92
#, kde-format
msgid "For install or remove, operates on applets installed for all users."
msgstr ""
"Diegimui bei šalinimui, dirba su visiems naudotojams įdiegtais įskiepiais."
"paketai"

#: amarokpkg.cpp:95
#, kde-format
msgctxt "Do not translate <path>"
msgid "Install the applet at <path>"
msgstr "Įdiegti įskiepį į <path>"

#: amarokpkg.cpp:97
#, kde-format
msgctxt "Do not translate <path>"
msgid "Upgrade the applet at <path>"
msgstr "Atnaujinti įskiepį, esantį <path>"

#: amarokpkg.cpp:99
#, kde-format
msgid "List installed applets"
msgstr "Įdiegtų įskiepių sąrašas"

#: amarokpkg.cpp:101
#, kde-format
msgctxt "Do not translate <name>"
msgid "Remove the applet named <name>"
msgstr "Pašalinti įskiepį vardu <name>"

#: amarokpkg.cpp:103
#, kde-format
msgid ""
"Absolute path to the package root. If not supplied, then the standard data "
"directories for this KDE session will be searched instead."
msgstr ""
"Absoliutus kelias iki paketų šaknies. Jei nebus nurodytas, bus ieškoma "
"standartiniuose šios sesijos duomenų aplankuose."

#: amarokpkg.cpp:162
#, kde-format
msgid "Successfully removed %1"
msgstr "Sėkmingai pašalintas %1"

#: amarokpkg.cpp:164
#, kde-format
msgid "Removal of %1 failed."
msgstr "%1 šalinimas nepavyko."

#: amarokpkg.cpp:169
#, kde-format
msgid "Plugin %1 is not installed."
msgstr "Priedas %1 nėra įdiegtas."

#: amarokpkg.cpp:174
#, kde-format
msgid "Successfully installed %1"
msgstr "Sėkmingai įdiegtas %1"

#: amarokpkg.cpp:177
#, kde-format
msgid "Installation of %1 failed."
msgstr "%1 diegimas nepavyko."

#: amarokpkg.cpp:183
#, kde-format
msgctxt ""
"No option was given, this is the error message telling the user he needs at "
"least one, do not translate install, remove, upgrade nor list"
msgid "One of install, remove, upgrade or list is required."
msgstr "Reikia nurodyti diegimą, šalinimą, atnaujinimą arba sąrašo rodymą."