# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 11:43+0100\n"
"PO-Revision-Date: 2010-03-10 00:05+0000\n"
"Last-Translator: Andrew Coles <andrew_coles@yahoo.co.uk>\n"
"Language-Team: British English <kde-i18n-doc@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrew Coles"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrew_coles@yahoo.co.uk"

#: amarokpkg.cpp:40
#, kde-format
msgid "Install, list, remove Amarok applets"
msgstr "Install, list, remove Amarok applets"

#: amarokpkg.cpp:76
#, kde-format
msgid "Amarok Applet Manager"
msgstr "Amarok Applet Manager"

#: amarokpkg.cpp:78
#, kde-format
msgid "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"
msgstr "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"

#: amarokpkg.cpp:79
#, kde-format
msgid "Aaron Seigo"
msgstr "Aaron Seigo"

#: amarokpkg.cpp:80
#, kde-format
msgid "Original author"
msgstr "Original author"

#: amarokpkg.cpp:82
#, kde-format
msgid "Leo Franchi"
msgstr "Leo Franchi"

#: amarokpkg.cpp:83
#, kde-format
msgid "Developer"
msgstr "Developer"

#: amarokpkg.cpp:92
#, kde-format
msgid "For install or remove, operates on applets installed for all users."
msgstr "For install or remove, operates on applets installed for all users."

#: amarokpkg.cpp:95
#, kde-format
msgctxt "Do not translate <path>"
msgid "Install the applet at <path>"
msgstr "Install the applet at <path>"

#: amarokpkg.cpp:97
#, kde-format
msgctxt "Do not translate <path>"
msgid "Upgrade the applet at <path>"
msgstr "Upgrade the applet at <path>"

#: amarokpkg.cpp:99
#, kde-format
msgid "List installed applets"
msgstr "List installed applets"

#: amarokpkg.cpp:101
#, kde-format
msgctxt "Do not translate <name>"
msgid "Remove the applet named <name>"
msgstr "Remove the applet named <name>"

#: amarokpkg.cpp:103
#, kde-format
msgid ""
"Absolute path to the package root. If not supplied, then the standard data "
"directories for this KDE session will be searched instead."
msgstr ""
"Absolute path to the package root. If not supplied, then the standard data "
"directories for this KDE session will be searched instead."

#: amarokpkg.cpp:162
#, kde-format
msgid "Successfully removed %1"
msgstr "Successfully removed %1"

#: amarokpkg.cpp:164
#, kde-format
msgid "Removal of %1 failed."
msgstr "Removal of %1 failed."

#: amarokpkg.cpp:169
#, kde-format
msgid "Plugin %1 is not installed."
msgstr "Plugin %1 is not installed."

#: amarokpkg.cpp:174
#, kde-format
msgid "Successfully installed %1"
msgstr "Successfully installed %1"

#: amarokpkg.cpp:177
#, kde-format
msgid "Installation of %1 failed."
msgstr "Installation of %1 failed."

#: amarokpkg.cpp:183
#, kde-format
msgctxt ""
"No option was given, this is the error message telling the user he needs at "
"least one, do not translate install, remove, upgrade nor list"
msgid "One of install, remove, upgrade or list is required."
msgstr "One of install, remove, upgrade or list is required."